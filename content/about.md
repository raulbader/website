---
title: "About me"
date: 2022-02-01
draft: false
---

Email
: [nzure@leaft.ch](mailto:nzure@leaft.ch)

PGP
: [pgp.asc](/pgp.asc)

Codeberg
: [codeberg.org/nzure](https://codeberg.org/nzure)

Gitlab
: [gitlab.com/nzure](https://gitlab.com/nzure)

Threema
: [44UYPCDN](https://threema.id/44UYPCDN)

I’m a young computer enthusiast from Switzerland
(I’m sure you’ve guessed that from the domain).
Naturally, this means that English is not my native language, so expect some
errors here and there (Feel free to contact me regarding any language mistakes).
I’m too young to study at the moment, but in the future I’d like study computer
science while traveling all around the globe.
If you want to know more about my technical side, head over to
[software that I use](/software).

{{< figure
	src="/avatar.webp"
	alt="Rin Tohsaka drinking coffe from a can."
	caption="My current avatar on most platforms. Please send me an email if you find the original source of this image, as I’d like to credit the author"
>}}

I started this website to have my own place in the web, because I don’t like big
tech and social media.
Everyone should have their own website.
You’d rather be a landlord than a peasant, right?
Im also against the massive usage of javascript, which is a sad trend that the
“modern” web has adopted.
