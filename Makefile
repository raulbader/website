
compile:
	hugo

serve:
	hugo server -D --disableFastRender

deploy: compile
	rsync -rtvzP public/ nzure@leaft.ch:~/www
