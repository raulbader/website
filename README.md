# Website
My personal website, built with hugo.
Hosted over at [leaft.ch/~nzure](https://leaft.ch/~nzure).

## Build
Build with make or hugo, as you wish.
